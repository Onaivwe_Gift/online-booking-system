/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;


/**
 *
 * @author User
 */
public class Specialist {
    
    private String Specialist_Id;
    private String Specialist_Name;
    private String[] Days;
    private ArrayList<Slot> Slots;
    private ArrayList<Appointment> Bookings;

    public Specialist(String Specialist_Id, String Specialist_Name, String[] Days) {
        this.Specialist_Id = Specialist_Id;
        this.Specialist_Name = Specialist_Name;
        this.Days = Days;
        this.Slots = new ArrayList<Slot>();
        for(String day: Days){
            this.Slots.add(new Slot(day, "08AM", true));
            this.Slots.add(new Slot(day, "10AM", true));
        }
        Bookings = new ArrayList<Appointment>();
    }

    public String getSpecialist_Id() {
        return Specialist_Id;
    }

    public void setSpecialist_Id(String Specialist_Id) {
        this.Specialist_Id = Specialist_Id;
    }

    public String getSpecialist_Name() {
        return Specialist_Name;
    }

    public void setSpecialist_Name(String Specialist_Name) {
        this.Specialist_Name = Specialist_Name;
    }

    public String[] getDays() {
        return Days;
    }

    public void setDays(String[] Days) {
        this.Days = Days;
    }

    public ArrayList<Slot> getSlots() {
        return Slots;
    }

    public void setSlots(ArrayList<Slot> Slots) {
        this.Slots = Slots;
    }
    
    public ArrayList<Slot> getAvailableSlot(){
        ArrayList<Slot> slots = new ArrayList<Slot>();
        for(Slot s: this.getSlots()){
            if(s.isAvailable()){
                slots.add(s);
            }
        }
        return slots;
    }

    public ArrayList<Appointment> getAppointments(ArrayList<Appointment> appointments){
        ArrayList<Appointment> appointments1 = new ArrayList<Appointment>();
        for(Appointment a: appointments){
            if(a.getSpecialist().equals(this)){
                appointments1.add(a);
            }
        }
        return appointments1;
    }
     
}
