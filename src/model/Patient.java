/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
/**
 *
 * @author User
 */
public class Patient {
    
    private int Hospital_Id;
    private String Name;

    public Patient(int Hospital_Id, String Name) {
        this.Hospital_Id = Hospital_Id;
        this.Name = Name;
    }

    
    public int getHospital_Id() {
        return Hospital_Id;
    }

    public void setHospital_Id(int Hospital_Id) {
        this.Hospital_Id = Hospital_Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }    
    
    public static Patient get(ArrayList<Patient> p, int id) {
        for(int i = 0; i < p.size(); i++){
            if(p.get(i).getHospital_Id() == id){
                return p.get(i);
            }
        }
        return null;
    }

    public ArrayList<Appointment> getAppointments(ArrayList<Appointment> appointments){
        ArrayList<Appointment> appointments1 = new ArrayList<Appointment>();
        for(Appointment a: appointments){
            if(a.getPatient().equals(this)){
                if(!a.isAttended())
                    appointments1.add(a);
            }
        }
        return appointments1;
    }

    public Appointment getAppointment(ArrayList<Appointment> appointments, String code){
        Appointment appointment = null;
        for(Appointment a: appointments){
            if(a.getPatient().equals(this)){
                if(!a.isAttended()){
                    if(a.getCode().equalsIgnoreCase(code)){
                        appointment = a;
                    }
                }
            }
        }
        return appointment;
    }
}
