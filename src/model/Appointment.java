/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author User
 */
public class Appointment 
{
    
    public static final String REGISTERED = "Registered";
    public static final String WAITING = "Waiting";
    public static final String IN_PROGRESS = "In Progress";
    public static final String FINISH = "Finish";
    public static final String CANCEL = "Cancelled";
    
    private Specialist specialist;
    private Patient patient;
    private String desired_Date;
    private String status;
    private String code;
    private boolean attended;
    private Slot slot;    

    public Appointment(Specialist Specialist, Patient Patient, String Desired_Date, Slot slot) {
        this.specialist = Specialist;
        this.patient = Patient;
        this.desired_Date = Desired_Date;
        this.status = REGISTERED;
        this.attended = false;
        this.slot = slot;
        this.slot.setAvailable(false);
        this.code = this.specialist.getSpecialist_Id() + this.slot.getCode();
    }

    public String getDesired_Date() {
        return desired_Date;
    }

    public void setDesired_Date(String Desired_Date) {
        this.desired_Date = Desired_Date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String Status) {
        this.status = Status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String Code) {
        this.code = Code;
    }

    public boolean isAttended() {
        return attended;
    }

    public void setAttended(boolean attended) {
        this.attended = attended;
    }
    
    public Specialist getSpecialist() {
        return specialist;
    }

    public void setSpecialist(Specialist Specialist) {
        this.specialist = Specialist;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient Patient) {
        this.patient = Patient;
    }
    
    public Slot getSlot() {
        return slot;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }
    
}
