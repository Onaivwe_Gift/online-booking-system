/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author emacodos
 */
public class Slot {
    
    private String day;
    private String time;
    private String code;
    private boolean available;

    public Slot(String day, String time, boolean isAvailable) {
        this.day = day;
        this.time = time;
        this.code = day+time;
        this.available = isAvailable;
    } 

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean isAvailable) {
        this.available = isAvailable;
    }
    
}
