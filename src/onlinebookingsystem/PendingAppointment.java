/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinebookingsystem;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author User
 */
public class PendingAppointment extends JFrame
{
    private JButton jbtClose;
    private JLabel jlbSlotCode;
    private JLabel jlbSlotStatus;
    private JLabel jlbSlotStatus1;
    private JLabel jlbSlotCode1;
    private JLabel jlbSlotCode2;
    private JLabel jlbSlotStatus2;
    private JPanel jplAvailableSlot;
    private JPanel jPanel2;
    
    PendingAppointment()
    {
       
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridLayout(2, 2));

        jplAvailableSlot.setLayout(new java.awt.GridLayout(3, 2));

        jlbSlotCode.setText("Slot Code");
        jplAvailableSlot.add(jlbSlotCode);

        jlbSlotStatus.setText("Slot Status");
        jplAvailableSlot.add(jlbSlotStatus);

        jlbSlotStatus1.setText("CM008AM");
        jplAvailableSlot.add(jlbSlotStatus1);

        jlbSlotCode1.setText("WAITING");
        jplAvailableSlot.add(jlbSlotCode1);

        jlbSlotCode2.setText("CM012PM");
        jplAvailableSlot.add(jlbSlotCode2);

        jlbSlotStatus2.setText("IN_PROGRESS");
        jplAvailableSlot.add(jlbSlotStatus2);

        getContentPane().add(jplAvailableSlot);

        jbtClose.setText("Close");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(327, Short.MAX_VALUE)
                .addComponent(jbtClose)
                .addGap(14, 14, 14))
        );
        jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jbtClose, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addContainerGap())
        );

        getContentPane().add(jPanel2);

        pack();
    }
    
}
