/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinebookingsystem;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author User
 */
public class Specialist extends JFrame
{
   JPanel pane = new JPanel();
    JLabel paneTitle = new JLabel("Select Specialist");
    JComboBox jcbSpecialistType = new JComboBox ();
    JButton jbtPatientSchedule = new JButton ("View all specialist appointment");
    
    Specialist()
    {
        for(model.Specialist s:OnlineBookingSystem.SPECIALISTS){
            jcbSpecialistType.addItem(s.getSpecialist_Name());
        }
        setTitle("Specialist");
        setBounds(500,100,700,300);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      Container con = this.getContentPane(); // inherit main frame
      con.add(pane);    // JPanel containers default to FlowLayout
      pane.add(paneTitle);
      pane.add(jcbSpecialistType); 
      pane.add(jbtPatientSchedule);
      
      jbtPatientSchedule.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                SpecialistAppiontment.specialist = OnlineBookingSystem.SPECIALISTS.get(jcbSpecialistType.getSelectedIndex());
                SpecialistAppiontment specialistAppiontmentFrame = new SpecialistAppiontment();
                specialistAppiontmentFrame.setBounds(500,100,400,200);
                specialistAppiontmentFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                dispose();
                
            }
        });
      
      pack();
        setVisible(true);

    }
    
}
