/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinebookingsystem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import model.Appointment;
import model.Patient;
import model.Slot;
import model.Specialist;

/**
 *
 * @author User
 */
public class AvailableSlots extends JFrame
{
    private ButtonGroup buttonGroup1;
    private JComboBox jcbSpecialistType;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JTextField jTextField1;
    private JButton jbtAppoint;
    
    ArrayList<Specialist> specialists = OnlineBookingSystem.SPECIALISTS;
    public static Specialist SP;
    public static Patient PA;    
    public static String DATE; 
    public static Slot SLOT;
    
    AvailableSlots() 
    {
        jPanel1 = new JPanel();
        jPanel2 = new JPanel();
        buttonGroup1 = new ButtonGroup();
        jcbSpecialistType = new JComboBox();
        jTextField1 = new JTextField();
        jbtAppoint = new JButton();
        jLabel6 = new JLabel();
        jLabel5 = new JLabel();
        jLabel4 = new JLabel();
        jLabel3 = new JLabel();
        
        
        
        
        jbtAppoint = new JButton("Save");
        
        ArrayList<Slot> slots = SP.getAvailableSlot();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Schedule Appoint");
        getContentPane().setLayout(new java.awt.GridLayout(2, 5));

        jPanel1.setLayout(new java.awt.GridLayout(5, 2));

        jLabel3.setText("Slot Code");
        jPanel1.add(jLabel3);

        jLabel4.setText("Slot Date");
        jPanel1.add(jLabel4);
        
        for(int i=0; i<slots.size(); i++){
            JRadioButton rb = new JRadioButton(slots.get(i).getCode());
            final Slot s = slots.get(i);
            rb.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    if(((JRadioButton)e.getSource()).isSelected()){
                        SLOT = s;
                    }
                }
            });
            JLabel la = new JLabel(slots.get(i).getDay());
            buttonGroup1.add(rb);
            jPanel1.add(rb);
            jPanel1.add(la);
        }

        getContentPane().add(jPanel1);

        jPanel2.setLayout(new java.awt.GridLayout(2, 2));
        jTextField1.setText(DATE);

        jLabel5.setText("Desired date:");
        jPanel2.add(jLabel5);
        jPanel2.add(jTextField1);

        jLabel6.setText("Select Specialist:");
        jPanel2.add(jLabel6);

        for(Specialist s:OnlineBookingSystem.SPECIALISTS){
            jcbSpecialistType.addItem(s.getSpecialist_Name());
        }
        jcbSpecialistType.setSelectedItem(SP.getSpecialist_Name());
        jcbSpecialistType.setEnabled(false);
        jcbSpecialistType.setMaximumSize(new java.awt.Dimension(327, 327));
        jcbSpecialistType.setMinimumSize(new java.awt.Dimension(0, 0));
        jPanel2.add(jcbSpecialistType);
        
        jbtAppoint.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {//getSelection().getSelectedObjects();
                Appointment a = new Appointment(SP, PA, DATE, SLOT);
                OnlineBookingSystem.APPOINTMENTS.add(a);
                System.out.println("Appointment is registered");
                JOptionPane.showMessageDialog(null, "Your Appointment Booking code is "  + a.getCode(), "Booking", JOptionPane.INFORMATION_MESSAGE);
                dispose();
            }
        });

        getContentPane().add(jPanel2);
        add(jbtAppoint);

        pack();
        
        setVisible(true);
         
    }
}
