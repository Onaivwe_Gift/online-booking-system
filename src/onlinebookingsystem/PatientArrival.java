/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinebookingsystem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import model.Appointment;
import model.Patient;

/**
 *
 * @author User
 */
public class PatientArrival extends JFrame
{
    private JButton jbtBookAppointment;
    private JLabel jlbHospitalId;
    private JLabel jblBookingCode;
    private JPanel jPanel1;
    private JTextField jtfHospitalId;
    private JTextField jtfBookingCode;
    
    PatientArrival()
    {
        jPanel1 = new JPanel();
        jlbHospitalId = new JLabel();
        jblBookingCode = new JLabel();
        jbtBookAppointment = new JButton();
        jtfHospitalId = new JTextField();
        jtfBookingCode = new JTextField();
        
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Patient Arrival");
        getContentPane().setLayout(new java.awt.GridLayout(2, 2));

        jPanel1.setLayout(new java.awt.GridLayout(2, 2));

        jlbHospitalId.setText("Hospital Id:");
        jPanel1.add(jlbHospitalId);
        jPanel1.add(jtfHospitalId);

        jblBookingCode.setText("Booking Code:");
        jPanel1.add(jblBookingCode);
        jPanel1.add(jtfBookingCode);

        getContentPane().add(jPanel1);

        jbtBookAppointment.setText("See Specialist");
        jbtBookAppointment.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String id = jtfHospitalId.getText();
                String code = jtfBookingCode.getText();
                Patient p = Patient.get(OnlineBookingSystem.PATIENTS, Integer.parseInt(id));
                Appointment a = p.getAppointment(OnlineBookingSystem.APPOINTMENTS, code);
                if (a!= null){
                   // JOptionPane.showMessageDialog(null, "Patient is: "  + " " +  (a.getStatus()) );
                    
                int dialogButton = JOptionPane.YES_NO_OPTION;
               // int dialogButton1 = JOptionPane.NO_OPTION;
                int dg = JOptionPane.showConfirmDialog (null, "Patient is Waiting... Do you want to cancel appointment?","Information",dialogButton);
                if(dg == JOptionPane.YES_OPTION)
                {
                    a.setStatus(Appointment.CANCEL);
                    System.out.println("Appointment has been " + a.getStatus() + "by Patient");
                } 
                else if(dg == JOptionPane.NO_OPTION)
                {
                    a.setStatus(Appointment.WAITING);
                    System.out.println("Patient is still " + a.getStatus());
                } 
                    
                }else{                    
                    System.out.println("Appointment Not Found");
                }
                dispose();
            }
        });
        getContentPane().add(jbtBookAppointment);

        pack();
        setVisible(true);
    }
}
