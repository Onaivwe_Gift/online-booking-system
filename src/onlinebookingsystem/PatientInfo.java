/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinebookingsystem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import model.Appointment;
import model.Patient;

/**
 *
 * @author User
 */
public class PatientInfo extends JFrame
{
    private JLabel jblPatId;
    private JLabel jblPatName;
    private JLabel jblViewPatId;
    private JLabel jblViewPatName;
    
    private JButton jbtCancel;
    private JButton jbtFinish;
    
    public static Patient PAT;
    public static Appointment APP;
    
    PatientInfo()
    {        
        jbtCancel = new JButton("Cancel");
        jbtFinish = new JButton("Finish");
        
        jblPatId = new JLabel();
        jblPatName = new JLabel();
        jblViewPatId = new JLabel();
        jblViewPatName = new JLabel();
        

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Patient Information");
        getContentPane().setLayout(new java.awt.GridLayout(3, 2));        

        jblPatId.setText("Patient ID");
        getContentPane().add(jblPatId);

        jblViewPatId.setText(PAT.getHospital_Id()+"");
        getContentPane().add(jblViewPatId);

        jblPatName.setText("Patient Name");
        getContentPane().add(jblPatName);

        jblViewPatName.setText(PAT.getName());
        getContentPane().add(jblViewPatName);
        
        if(APP.getStatus().equals(Appointment.WAITING) || APP.getStatus().equals(Appointment.IN_PROGRESS)){
            APP.setStatus(Appointment.IN_PROGRESS);
            getContentPane().add(jbtFinish);
        }
        else{
            getContentPane().add(jbtCancel);            
        }
        
        jbtCancel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                APP.getSlot().setAvailable(true);
                APP.setStatus(Appointment.CANCEL);
                System.out.println("Appointment Cancelled");
                dispose();
            }
        });
        
        jbtFinish.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                APP.getSlot().setAvailable(true);
                APP.setStatus(Appointment.FINISH);
                System.out.println("Appointment Finished");
                dispose();
            }
        });

        pack();        
        setVisible(true);
    }
    
}
