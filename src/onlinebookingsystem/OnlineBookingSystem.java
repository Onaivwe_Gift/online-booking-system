/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinebookingsystem;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import model.Appointment;
import model.Patient;
import model.Specialist;

/**
 *
 * @author User
 */
public class OnlineBookingSystem extends JFrame 
{
    
    
    JPanel pane = new JPanel();
    JLabel paneTitle = new JLabel("Welcome to Newgate Online Booking System.");
    JLabel paneNavigationInfo = new JLabel("Please use below buttons to navigate by clicking on it.");
    JButton jbtPatientSchedule = new JButton ("Patient Schedule");
    JButton jbtPatientArrival = new JButton ("Patient Arrival");
    JButton jbtSpecialist = new JButton ("Specialist");
    JButton jbtAdministrator = new JButton ("Administrator");
   // JComboBox<String> SpecialistTy = new JComboBox<>(Specialist_Type);
    
   // jbtPatientSchedule.
    public static final ArrayList<Specialist> SPECIALISTS = new ArrayList<Specialist>();
    public static final ArrayList<Patient> PATIENTS = new ArrayList<Patient>();    
    public static final ArrayList<Appointment> APPOINTMENTS = new ArrayList<Appointment>();
         
    OnlineBookingSystem()        // the frame constructor
    {
        
      //super("Online System"); 
      setTitle("Newgate Online Booking System.");
      setBounds(500,100,400,300);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      Container con = this.getContentPane(); // inherit main frame
      con.add(pane);    // JPanel containers default to FlowLayout
      pane.add(paneTitle);
      pane.add(paneNavigationInfo);
      pane.add(jbtPatientSchedule);
      pane.add(jbtPatientArrival);
      pane.add(jbtSpecialist);      
      pane.add(jbtAdministrator);
      
      jbtPatientSchedule.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              
               PatientSchedule patientScheduleFrame = new PatientSchedule();
               patientScheduleFrame.setBounds(500,100,550,600);
               patientScheduleFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
               
            }
        });
      
      jbtAdministrator.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              
               Admin patientScheduleFrame = new Admin();
               patientScheduleFrame.setBounds(500,100,400,200);
               patientScheduleFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
               
            }
        });
      
      jbtPatientArrival.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              
               PatientArrival patientArrivalFrame = new PatientArrival();
               patientArrivalFrame.setBounds(500,100,250,200);
               patientArrivalFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
               
            }
        });
      
      jbtSpecialist.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              
                onlinebookingsystem.Specialist specialistFrame = new onlinebookingsystem.Specialist();
                specialistFrame.setBounds(500,100,420,150);
                specialistFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                
            }
        });
      
      
      setVisible(true); // make frame visible
      
    }
  
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) 
    { 
        SPECIALISTS.add(new Specialist("C", "Cardiac Specialist", new String[]{"MO","TU"}));
        SPECIALISTS.add(new Specialist("R", "Renal Specialist", new String[]{"MO","FR"}));
        SPECIALISTS.add(new Specialist("A", "Allergist Specialist", new String[]{"MO","TU"}));
        SPECIALISTS.add(new Specialist("E", "ENT Specialist", new String[]{"WE","TH"}));
        SPECIALISTS.add(new Specialist("P", "Paediatric Specialist", new String[]{"TH","FR"}));
        
        PATIENTS.add(new Patient(001, "Mary"));
        PATIENTS.add(new Patient(002, "Stanley"));
        PATIENTS.add(new Patient(001, "Gift"));
        
        APPOINTMENTS.add(new Appointment(SPECIALISTS.get(0), PATIENTS.get(0), "09-04-2015", SPECIALISTS.get(0).getSlots().get(0)));
        APPOINTMENTS.add(new Appointment(SPECIALISTS.get(0), PATIENTS.get(2), "09-06-2015", SPECIALISTS.get(0).getSlots().get(1)));
        
        APPOINTMENTS.add(new Appointment(SPECIALISTS.get(1), PATIENTS.get(1), "09-04-2015", SPECIALISTS.get(1).getSlots().get(0)));
        APPOINTMENTS.add(new Appointment(SPECIALISTS.get(1), PATIENTS.get(2), "04-06-2015", SPECIALISTS.get(1).getSlots().get(1)));
        
        APPOINTMENTS.add(new Appointment(SPECIALISTS.get(2), PATIENTS.get(2), "09-04-2015", SPECIALISTS.get(2).getSlots().get(0)));
        APPOINTMENTS.add(new Appointment(SPECIALISTS.get(2), PATIENTS.get(1), "09-06-2015", SPECIALISTS.get(2).getSlots().get(1)));
        

        JFrame frame = new OnlineBookingSystem();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }
}
