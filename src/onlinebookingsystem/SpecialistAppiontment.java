/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinebookingsystem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import model.Appointment;
import model.Patient;

/**
 *
 * @author User
 */
public class SpecialistAppiontment extends JFrame
{
    private JButton jbtViewPatient1;
    private JButton jbtViewPatient2;
    private JButton jbtViewPatient3;
    private JLabel jlbPatientCode;
    private JLabel jlbPatientStatus;
    private JLabel jblViewPatient;
    private JLabel jlbPatientCode1;
    private JLabel jlbPatientStatus1;
    private JLabel jlbPatientCode2;
    private JLabel jlbPatientStatus2;
    private JLabel jlbPatientCode3;
    private javax.swing.JLabel jlbPatientStatus3;
    
    public static model.Specialist specialist;
    
    SpecialistAppiontment()
    {
        jbtViewPatient1 = new JButton();
        jbtViewPatient2 = new JButton();
        jbtViewPatient3 = new JButton();
        jlbPatientCode = new JLabel();
        jlbPatientStatus = new JLabel();
        jblViewPatient = new JLabel();
        
        
        setTitle("Specialist to view all appointment");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridLayout(4, 3));

        jlbPatientCode.setText("Booking Code");
        getContentPane().add(jlbPatientCode);

        jlbPatientStatus.setText("Status");
        getContentPane().add(jlbPatientStatus);

        jblViewPatient.setText("View Appointment");
        getContentPane().add(jblViewPatient);
        
        for (final Appointment a : specialist.getAppointments(OnlineBookingSystem.APPOINTMENTS)){
            JLabel jlbCode = new JLabel(a.getCode());
            JLabel jlbStatus = new JLabel(a.getStatus());
            final JButton jbtPatient = new JButton("View Patient");
            if(a.getStatus().equals(Appointment.WAITING)){
                jbtPatient.setText("Attend Patient");
            }
            final Patient pa = a.getPatient();
            jbtPatient.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                PatientInfo.PAT = pa;
                PatientInfo.APP = a;
                PatientInfo patientInfoFrame = new PatientInfo();
                patientInfoFrame.setBounds(500,100,400,200);
                patientInfoFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                if(jbtPatient.getText().equalsIgnoreCase("Attend Patient")){
                    System.out.println("Patient Appointment is in progress");
                }
                dispose();
            }
        });
            getContentPane().add(jlbCode);
            getContentPane().add(jlbStatus);
            getContentPane().add(jbtPatient);
        }
        
        jbtViewPatient1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              
                PatientInfo patientInfoFrame = new PatientInfo();
                patientInfoFrame.setBounds(500,100,400,200);
                patientInfoFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                
            }
        });
        
        jbtViewPatient2.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              
                PatientInfo patientInfoFrame = new PatientInfo();
                patientInfoFrame.setBounds(500,100,400,200);
                patientInfoFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                
            }
        });
        
        jbtViewPatient3.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              
                PatientInfo patientInfoFrame = new PatientInfo();
                patientInfoFrame.setBounds(500,100,400,200);
                patientInfoFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                
            }
        });

        pack();
        setVisible(true);
    }
    
}
